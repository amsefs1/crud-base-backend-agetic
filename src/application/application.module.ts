import { Module } from '@nestjs/common'
import { ParametricasModule } from './parametricas/parametricas.module'
import { ProductoModule } from './productos/producto.module'
@Module({
  imports: [ParametricasModule, ProductoModule],
})
export class ApplicationModule {}
