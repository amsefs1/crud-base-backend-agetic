import { Status } from 'src/common/constants'
import { AuditoriaEntity } from 'src/common/entity/auditoria.entity'
import { UtilService } from 'src/common/lib/util.service'
import { Check, Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

export const ProductoEstado = {
  ACTIVE: Status.ACTIVE,
  INACTIVE: Status.INACTIVE,
}

@Check(UtilService.buildStatusCheck(ProductoEstado))
@Entity({ name: 'productos', schema: process.env.DB_SCHEMA_PARAMETRICAS })
export class Producto extends AuditoriaEntity {
  @PrimaryGeneratedColumn({
    name: 'id',
    type: 'bigint',
  })
  id: string

  @Column({
    length: 100,
    type: 'varchar',
    unique: true,
  })
  nombre: string

  @Column({
    type: 'int',
  })
  precio: number

  @Column({
    type: 'int',
  })
  cantidad: number

  constructor(data?: Partial<Producto>) {
    super(data)
  }

  insertarEstado() {
    this.estado = this.estado || ProductoEstado.ACTIVE
  }
}
