import { Injectable, NotFoundException } from '@nestjs/common'
import { BaseService } from 'src/common/base'
import { Messages } from 'src/common/constants/response-messages'
import { ActualizarProductoDto } from '../dto/actualizar-producto.dto'
import { CrearProductoDto } from '../dto/crear-producto.dto'
import { ProductoEstado } from '../entity/producto.entity'
import { ProductoRepository } from '../repository/producto.repository'

@Injectable()
export class ProductoService extends BaseService {
  constructor(private readonly productoRepositorio: ProductoRepository) {
    super()
  }

  async crearProducto(datos: CrearProductoDto) {
    const result = await this.productoRepositorio.crearProducto(datos)
    return result
  }

  async obtenerPorId(idProducto: string) {
    const result = await this.productoRepositorio.buscarPorId(idProducto)
    return result
  }

  async actualizarProducto(idProducto: string, datos: CrearProductoDto) {
    const result = await this.productoRepositorio.actualizarProducto(
      idProducto,
      datos
    )
    return result
  }

  async inactivar(idProducto: string) {
    console.log('idProducto-service-->', idProducto)
    const producto = await this.productoRepositorio.buscarPorId(idProducto)
    if (!producto) {
      console.log('producto::::', producto)
      throw new NotFoundException(Messages.EXCEPTION_DEFAULT)
    }
    const productoDto = new ActualizarProductoDto()
    productoDto.estado = ProductoEstado.INACTIVE
    await this.productoRepositorio.actualizarProducto(idProducto, productoDto)
    return { id: idProducto, estado: productoDto.estado }
  }
}
