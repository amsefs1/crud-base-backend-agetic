import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { ProductoController } from './controller/producto.controller'
import { ProductoRepository } from './repository/producto.repository'
import { ProductoService } from './service/producto.service'
import { Producto } from './entity/producto.entity'

@Module({
  controllers: [ProductoController],
  providers: [ProductoService, ProductoRepository],
  imports: [TypeOrmModule.forFeature([Producto])],
  exports: [ProductoRepository, ProductoService],
})
export class ProductoModule {}
