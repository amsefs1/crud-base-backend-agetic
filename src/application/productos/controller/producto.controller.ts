import { Body, Controller, Get, Param, Patch, Post, Req } from '@nestjs/common'
import { BaseController } from 'src/common/base'
import { ProductoService } from '../service/producto.service'
import { ParamIdDto } from '../dto/param-id.dto'
import { CrearProductoDto } from '../dto/crear-producto.dto'
import { ActualizarProductoDto } from '../dto/actualizar-producto.dto'
@Controller('productos')
export class ProductoController extends BaseController {
  constructor(private productoService: ProductoService) {
    super()
  }
  @Get('/:id')
  async obtener(@Param() param: ParamIdDto) {
    console.log('id::', param.id)
    const result = await this.productoService.obtenerPorId(param.id)
    return result
  }

  @Post()
  async Crear(@Body() datos: CrearProductoDto) {
    console.log('DATOS', datos)
    const result = await this.productoService.crearProducto(datos)
    return result
  }

  @Patch(':id')
  async actualizar(
    @Param() params: ParamIdDto,
    @Req() req: Request,
    @Body() parametroDto: ActualizarProductoDto
  ) {
    const { id: idProducto } = params
    const result = await this.productoService.actualizarProducto(
      idProducto,
      parametroDto
    )
    return this.successUpdate(result)
  }

  @Patch('/:id/inactivacion')
  async inactivar(@Req() req: Request, @Param() params: ParamIdDto) {
    const { id: idProducto } = params
    const result = await this.productoService.inactivar(idProducto)
    return this.successUpdate(result)
  }
}
