import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty } from 'class-validator'

export class CrearProductoDto {
  @ApiProperty({ example: 'colchones' })
  @IsNotEmpty()
  nombre: string

  @ApiProperty({ example: '5' })
  @IsNotEmpty()
  cantidad: number

  @ApiProperty({ example: '5.3' })
  @IsNotEmpty()
  precio: number
}
