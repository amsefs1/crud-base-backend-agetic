import { Injectable } from '@nestjs/common'
import { DataSource } from 'typeorm'
import { CrearProductoDto } from '../dto/crear-producto.dto'
import { Producto, ProductoEstado } from '../entity/producto.entity'

@Injectable()
export class ProductoRepository {
  constructor(private datasource: DataSource) {}
  async crearProducto(datos_: CrearProductoDto) {
    console.log('REPOSITORY--->', datos_)
    const producto = new Producto()
    producto.nombre = datos_.nombre
    producto.cantidad = datos_.cantidad
    producto.precio = datos_.precio
    producto.estado = ProductoEstado.ACTIVE
    producto.usuarioCreacion = '1'

    return await this.datasource.getRepository(Producto).save(producto)
  }

  async buscarPorId(idProducto: string) {
    console.log('IDPRODUCTO:::', idProducto)
    const response = await this.datasource
      .getRepository(Producto)
      .createQueryBuilder('producto')
      .where({ id: idProducto })
      .getOne()
    return response
  }

  async actualizarProducto(idProducto: string, datos: CrearProductoDto) {
    const response = await this.datasource
      .getRepository(Producto)
      .update(idProducto, { ...datos })
    return response
  }
}
